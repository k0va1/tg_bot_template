module Commands
  Error = Class.new(StandardError)
  FallbackError = Class.new(Error)
end
