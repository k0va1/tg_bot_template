require_relative "./base"

module Commands
  class AddTranslation < Base
    private

    def handle_call(message)
      word = current_user.words.includes(:translations).where(translations: {id: nil}).first
      ::Services::AddTranslation.call(user: current_user, word: word, value: message.text)
      total_words = current_user.words.count
      send_message(
        chat_id: message.chat.id,
        text: text(total_words),
        parse_mode: :markdown,
        reply_markup: Keyboards::StartKeyboard.new.call
      )
    end

    def text(total_words)
      "Word added! You have #{total_words} #{'word'.pluralize(total_words)} in dictionary"
    end
  end
end
