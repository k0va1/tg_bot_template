require_relative "./base"

module Commands
  class AddWord < Base
    TEXT = "Enter translation:".freeze

    private

    def handle_call(message)
      ::Services::AddNewWord.call(user: current_user, word: message.text)

      send_message(
        chat_id: message.chat.id,
        text: TEXT,
        parse_mode: :markdown,
      )
    end
  end
end
