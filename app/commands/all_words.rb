require_relative "./base"

module Commands
  class AllWords < Base
    private

    def handle_call(message)
      send_message(
        chat_id: message.chat.id,
        text: text,
        parse_mode: :markdown,
        reply_markup: Keyboards::StartKeyboard.new.call
      )
    end

    def text
      words = current_user.words.includes(:translations).map do |word|
        [word.value, word.translations.first.value].join(" - ")
      end.join("\n")

      "You dictionary:\n#{words}"
    end
  end
end
