require 'forwardable'
require_relative "../../config/application_logger"

module Commands
  class Base
    extend Forwardable

    attr_reader :api

    def initialize(api)
      @api = api
      @logger = ApplicationLogger.new
    end

    def call(message)
      set_current_user(message)

      case message
      when Telegram::Bot::Types::Message
        handle_call(message)
      when Telegram::Bot::Types::CallbackQuery
        args = JSON.parse(message.data)['args']
        handle_callback(message, args)
      end
    rescue Services::Error => e
      @logger.error("Error: #{e.message}\n#{e.backtrace.join("\n")}")
      send_error_message(message.chat.id)
    rescue Telegram::Bot::Exceptions::ResponseError => e
      @logger.error("Error: #{e.message}\n#{e.backtrace.join("\n")}")
    end

    private

    def_delegators :api, :send_message, :edit_message_text,
      :send_location, :delete_message, :send_photo

    def handle_call(message)
      raise NotImplementedError, "you have to implement #{self.class.name}#handle_call"
    end

    def handle_callback(callback, args)
      raise NotImplementedError, "you have to implement #{self.class.name}#handle_callback"
    end

    def send_error_message(chat_id)
      send_message(
        chat_id: chat_id,
        text: "Something went wrong. Try again!",
        parse_mode: :markdown
      )
    end

    def set_current_user(message)
      from = message.from

      tg_id = from.id
      @current_user = ::Models::User.find_or_create_by(tg_id: tg_id) do |user|
        user.name = from.username || "#{from.first_name} #{from.last_name}"
      end
    end

    def current_user
      @current_user
    end
  end
end
