require_relative "./base"

module Commands
  class NewWord < Base
    TEXT = "Enter new english word:".freeze

    private

    def handle_call(message)
      @current_user.new_word!

      send_message(
        chat_id: message.chat.id,
        text: TEXT,
        parse_mode: :markdown,
        reply_markup: Keyboards::HideKeyboard.new.call
      )
    end
  end
end
