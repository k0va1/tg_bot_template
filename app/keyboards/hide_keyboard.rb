require_relative "./keyboard_helpers"

module Keyboards
  class HideKeyboard
    include KeyboardHelpers

    def call
      hide_keyboard
    end
  end
end
