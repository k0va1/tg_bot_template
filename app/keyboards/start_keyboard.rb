require_relative "./keyboard_helpers"

module Keyboards
  class StartKeyboard
    include KeyboardHelpers

    def call
      reply_keyboard(
        [button('✍️ New word'), button('📘 All words'), button("Start repeating")]
      )
    end
  end
end
