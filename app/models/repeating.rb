require "active_record"

module Models
  class Repeating < ActiveRecord::Base
    belongs_to :user
    has_many :word_repeatings
    has_many :words, through: :word_repeatings

    include AASM

    aasm column: :step do
      state :step_1, initial: true
      (2..10).to_a.each do |i|
        state :"step_#{i}"
      end
      state :finished

      event :next_step do
        transitions from: :idle, to: :new_word
      end

      event :finish do
        transitions to: :finished
      end
    end
  end
end
