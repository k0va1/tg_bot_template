require "active_record"

module Models
  class Translation < ActiveRecord::Base
    belongs_to :word

    validates :value, presence: true
  end
end
