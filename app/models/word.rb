require "active_record"

module Models
  class Word < ActiveRecord::Base
    has_many :translations, dependent: :destroy

    validates :value, presence: true
  end
end
