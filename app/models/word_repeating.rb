require "active_record"

module Models
  class WordRepeating < ActiveRecord::Base
    belongs_to :repeating
    belongs_to :word
  end
end
