class RequestDispatcher
  def initialize(bot)
    @bot = bot
    @fallback = Commands::Unknown.new(@bot.api)

    @commands = {
      ['/start'] => Commands::Start.new(@bot.api),
      ['/new_word', '✍️ New word'] => Commands::NewWord.new(@bot.api),
      ['/all_words', '📘 All words'] => Commands::AllWords.new(@bot.api),
    }
  end

  def call(message)
    case message
    when Telegram::Bot::Types::Message
      dispatch_message(message)
    when Telegram::Bot::Types::CallbackQuery
      dispatch_callback(message)
    end
  end

  private

  attr_reader :commands, :callbacks, :fallback

  def dispatch_message(message)
    current_user = Utils::CurrentUser.new.call(message)
    if !current_user.idle?
      process_word_adding(current_user, message)
    else
      command_name = message.text&.sub(/_\d+$/, '')
      handler = fetch_handler(command_name, fallback)
      handler.call(message)
    end
  end

  def fetch_handler(command_name, fallback)
    key = @commands.keys.select { |k| k.include?(command_name)}.flatten
    commands.fetch(key, fallback)
  end

  def dispatch_callback(callback)
    callback_name = JSON.parse(callback.data)['command']
    handler = callbacks.fetch(callback_name, fallback)

    handler.call(callback)
  rescue JSON::ParserError, Commands::FallbackError
    fallback.call(callback)
  end

  def process_word_adding(current_user, message)
    if current_user.new_word?
      Commands::AddWord.new(@bot.api).call(message)
    elsif current_user.new_translation?
      Commands::AddTranslation.new(@bot.api).call(message)
    end
  end
end
