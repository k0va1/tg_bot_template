require_relative "./base"

module Services
  class AddNewWord < Base
    def call(user:, word:)
      user.words.create!(value: word)
      user.add_word!
      user.new_translation!
    rescue ActiveRecord::RecordInvalid => exception
      raise ::Services::Error.new(exception.message)
    end
  end
end
