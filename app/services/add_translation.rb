require_relative "./base"

module Services
  class AddTranslation < Base
    def call(user:, word:, value:)
      word.translations.create!(value: value)
      user.add_translation!
      user.finish!
    rescue ActiveRecord::RecordInvalid => exception
      raise ::Services::Error.new(exception.message)
    end
  end
end
