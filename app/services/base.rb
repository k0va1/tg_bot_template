module Services
  class Error < StandardError
  end

  class Base
    def self.call(**args)
      new.call(**args)
    end
  end
end
