module Utils
  class CurrentUser
    def call(message)
      from = message.from

      tg_id = from.id
      ::Models::User.find_or_create_by(tg_id: tg_id) do |user|
        user.name = from.username || "#{from.first_name} #{from.last_name}"
      end
    end
  end
end
