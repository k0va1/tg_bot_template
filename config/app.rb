require 'bundler'

Bundler.require
Dotenv.load

require_relative "./application_logger"
require 'active_record'

env = ENV["APP_ENV"] || 'development'
db_config = YAML::load(File.open("config/database.yml"))[env]
ActiveRecord::Base.establish_connection(db_config)

root = Bundler.root
Dir.glob(root.join('app/**/*.rb')).each { |f| require f }
