class AddUsers < ActiveRecord::Migration[6.0]
  def change
    create_table(:users) do |t|
      t.string :name, null: false
      t.string :tg_id, null: false

      t.timestamps
    end
  end
end
