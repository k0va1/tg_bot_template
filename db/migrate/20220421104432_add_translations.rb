class AddTranslations < ActiveRecord::Migration[6.0]
  def change
    create_table(:translations) do |t|
      t.string :value, null: false
      t.references :word, foreign_key: true

      t.timestamps
    end
  end
end
